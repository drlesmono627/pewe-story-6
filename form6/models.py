from django.db.models import *
import datetime
import os

# Create your models here.

class StoryTime(Model):
    name = CharField(max_length=100)
    waktu_cerita = DateField(auto_now_add=True)
    cerita = TextField(max_length=300)

    def __str__(self):
        return self.name

    
