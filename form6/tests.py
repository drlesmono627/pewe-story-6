from django.test import TestCase
from django.test import Client
from django.urls import resolve
from landing_page.views import landing_page, redirecting
from django.http import HttpRequest
from form6.models import StoryTime
from form6.forms import addStories

# Create your tests here.
class Story6UnitTest(TestCase):
    
    def test_landing_page_url_is_exist(self):
        response = Client().get('/landing-page/')
        self.assertEqual(response.status_code, 200)

    def test_notexist_url_is_notexist(self):
    	response = Client().get('/profile/')
    	self.assertEqual(response.status_code, 404)

    def test_landingpage_using_landingpage_function(self):
    	response = resolve('/landing-page/')
    	self.assertEqual(response.func, landing_page)

    def test_landingpage_using_landingpage_template(self):
    	response = Client().get('/landing-page/')
    	self.assertTemplateUsed(response, 'index.html')

    def test_landing_page_is_completed(self):
    	request = HttpRequest()
    	response = landing_page(request)
    	html_response = response.content.decode('utf8')
    	self.assertIn('Halo, apa kabar?', html_response)

    def test_landing_page_title_is_right(self):
    	request = HttpRequest()
    	response = landing_page(request)
    	html_response = response.content.decode('utf8')
    	self.assertIn('<title>RAD</title>', html_response)

    #Redirect landing page
    def test_landing_page_using_redirecting_function(self):
    	response = resolve('/')
    	self.assertEqual(response.func, redirecting)

    def test_landing_page_redirecting(self):
    	response = Client().get('/')
    	self.assertEqual(response.status_code, 302)

    def test_landing_page_redirected_to_home(self):
    	response = Client().get('/')
    	self.assertRedirects(response, '/landing-page/')

	#SetUp and Models    
    def setUp(cls):
    	StoryTime.objects.create(status="Semangat")

    def test_if_models_in_database(self):
    	modelsObject = StoryTime.objects.create(status="Mantap")
    	count_Object = StoryTime.objects.all().count()
    	self.assertEqual(count_Object, 2)

    def test_if_StoryTime_status_is_exist(self):
    	modelsObject = StoryTime.objects.get(id=1)
    	statusObject = StoryTime._meta.get_field('status').verbose_name
    	self.assertEqual(statusObject, 'status')

    #Forms
    def test_forms_input_html(self):
    	form = addStories()
    	self.assertIn('id="id_status', form.as_p())

    def test_forms_validation_blank(self):
    	form = addStories(data={'status':''})
    	self.assertFalse(form.is_valid())
    	self.assertEquals(form.errors['status'], ["This field is required."])

    #Forms in templates
    def test_forms_in_template(self):
    	request = HttpRequest()
    	response = landing_page(request)
    	html_response = response.content.decode('utf8')
    	self.assertIn('<form method="POST" class="register_form">', html_response)

