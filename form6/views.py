from django.shortcuts import render, redirect

from form6.models import StoryTime
from form6.forms import addStories

# Create your views here.
def addStory(request):
     if (request.method == "POST"):
        forms = addStories(request.POST)
        if(forms.is_valid()):
            instance = forms.save()
            instance.save()
            return redirect("/")
    else:
		forms = addStories()

	cerita = StoryTime.objects.order_by('-date')
  return render(request, 'index.html', {'forms': forms, 'cerita': cerita})