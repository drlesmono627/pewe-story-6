from django.apps import AppConfig


class Form6Config(AppConfig):
    name = 'form6'
