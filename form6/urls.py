from django.urls import path
from .views import addStory

appname = 'form6'

urlpatterns = [
    path('', addStory, name='addstory'),
]