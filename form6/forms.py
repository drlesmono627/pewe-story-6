from django import forms
from django.utils.translation import gettext_lazy as _
from form6.models import StoryTime

class addStories(forms.ModelForm):
    class Meta:
        model = StoryTime
        fields = ['name', 'cerita']
        labels = {
                'name': _("Name"),
                'cerita' : _("Apa yang ingin kamu ceritakan hari ini?")
                }